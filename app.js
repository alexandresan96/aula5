const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
//body parser
//app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
    console.log("CHAMADO VIA GET!");
    res.send("Method GET - Route: /");
});

app.get('/usuario', (req, res) => {
    console.log("CHAMADO VIA GET - consulta usuario");
    res.send("Method GET - Route: /usuario");
});

app.get('/cliente', (req, res) => {
    console.log("CHAMADO VIA GET - consulta cliente");
    console.log(req.query);
    let nome = req.query.fname;
    let sobrenome = req.query.lname;
    let retorno;
    retorno = "<h1>Nome:</h1>" + nome + "<br/>";
    retorno += "<h1>Sobrenome:</h1>" + sobrenome;
    res.send(retorno);
});

app.post('/cliente', (req, res) => {
    console.log("Method POST - Route: /cliente");
    res.send("MÃ©todo post utilizado");
});

app.delete('/cliente', (req, res) => {
    console.log("Method DELETE - Route: /cliente");
    res.send("Metodo delete utilizado");
});

app.listen(8000, function () {
    console.log("Projeto iniciado na porta 8000");
});