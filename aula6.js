const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());


app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
    console.log("CHAMADO VIA GET!");
    res.send("Method GET - Route: /");
});

app.get('/login', (req, res) => {
    console.log("CHAMADO VIA GET! (CONSULTA USUÁRIO)");
    console.log("Method GET - Route: ");
    console.log(req.query);
    let usuario = req.query.user;
    let senha = req.query.password;
    let mostrar;

    mostrar = '<h1>E-mail:</h1>'+usuario+'<br>';
    mostrar += '<h1>Senha:</h1>'+senha+'<br>';
    res.send(mostrar);

});

app.post('/login', (req, res) => {
    console.log("Method POST - Route: /cliente");
    console.log("Método post utilizado");
    console.log(req.body);

    let nome = req.body.name;
    let sobrenome = req.body.lastname;
    let email = req.body.email;
    let aniversario = req.body.aniversario;
    let senha2 = req.body.password2;
    let mostrar;


    mostrar = '<h1>Nome:</h1>'+nome+'<br>';
    mostrar += '<h1>Sobrenome:</h1>'+sobrenome+'<br>';
    mostrar += '<h1>Email:</h1>'+email+'<br>';
    mostrar += '<h1>Aniversario:</h1>'+aniversario+'<br>';
    mostrar += '<h1>Senha:</h1>'+senha2+'<br>';
    res.send(mostrar);
    
});

app.listen(8000, function () {
    console.log("projeto iniciado na porta 8000");
});