const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
//body parser
app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
    console.log("CHAMADO VIA GET!");
    res.send("Method GET - Route: /");
});

app.get('/consultarpet', (req, res) => {
    console.log("CHAMADO VIA GET - consulta PET");
    console.log(req.query);
    let raca = req.query.raca;
    let idade = req.query.idade;
    let nome = req.query.nome;
    let ativo = req.query.ativo;
    let id = req.query.id;
    let retorno;
    retorno = "<h1>Raça:</h1>" + raca + "<br/>";
    retorno += "<h1>Idade:</h1>" + idade + "<br/>";
    retorno += "<h1>Nome:</h1>" + nome + "<br/>";
    retorno += "<h1>ID:</h1>" + id + "<br/>";
    retorno += "<h1>Ativo:</h1>" + ativo;
    res.send(retorno);
});

app.post('/cadastrarpet', (req, res) => {
    console.log("CHAMADO VIA POST - inserindo PET");
    console.log(req.body);
    let raca = req.body.raca;
    let idade = req.body.idade;
    let nome = req.body.nome;
    let ativo = req.body.ativo;
    let id = req.body.id;
    let retorno;

    retorno = "<h1>Raça:</h1>" + raca + "<br/>";
    retorno += "<h1>Idade:</h1>" + idade + "<br/>";
    retorno += "<h1>Nome:</h1>" + nome + "<br/>";
    retorno += "<h1>ID:</h1>" + id + "<br/>";
    retorno += "<h1>Ativo:</h1>" + ativo;
    res.send(retorno);
});

app.put('/inativarpet', (req, res) => {
    console.log("CHAMADO VIA PUT - inativando PET");
    console.log(req.body);
    console.log(req.params);

    let ativo = req.body.ativo;
    let id = req.params.id;
    let retorno;

    retorno = "<h1>ID:</h1>" + id + "<br/>";
    retorno += "<h1>Ativo:</h1>" + ativo;
    res.send(retorno);
});

app.put('/alterarpet', (req, res) => {
    console.log("CHAMADO VIA PUT - alterando PET");
    console.log(req.body);
    console.log(req.params);

    let raca = req.body.raca;
    let idade = req.body.idade;
    let nome = req.body.nome;
    let id = req.params.id;
    let retorno;

    retorno = "<h1>Raça:</h1>" + raca + "<br/>";
    retorno += "<h1>Idade:</h1>" + idade + "<br/>";
    retorno += "<h1>Nome:</h1>" + nome + "<br/>";
    retorno += "<h1>ID:</h1>" + id + "<br/>";
    res.send(retorno);
});

/*app.delete('/cliente', (req, res) => {
    console.log("Method DELETE - Route: /cliente");
    res.send("Metodo delete utilizado");
});*/

app.listen(8000, function () {
    console.log("Projeto iniciado na porta 8000");
});